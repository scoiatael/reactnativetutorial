import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TouchableHighlight
} from 'react-native';

const styles = require('./styles.js');

const SuccessImg = () => {
  return (
    <Image source={require('../img/check.png')} />
  );
}

const LoadingImg = () => {
  return (
    <Image source={require('../img/loading.gif')} />
  );
}

const Loading = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        Welcome to Foo Native!
      </Text>
      <LoadingImg />
    </View>
  );
}

const Loaded = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        Loaded!
      </Text>
      <SuccessImg style={styles.image}/>
    </View>
  );
}

const Movie = ({movie}) => {
  return (
     <View style={styles.container}>
       <Image
         source={{uri: movie.posters.thumbnail}}
         style={styles.thumbnail}
       />
       <View style={styles.rightContainer}>
         <Text style={styles.title}>{movie.title}</Text>
         <Text style={styles.year}>{movie.year}</Text>
       </View>
     </View>
   );
}

const DetailedMovie = ({movie}) => {
  return (
     <View style={styles.container}>
       <View style={styles.rightContainer}>
         <Text style={styles.title}>{movie.title}</Text>
         <Text style={styles.year}>{movie.year}</Text>
       </View>
       <Image
         source={{uri: movie.posters.detailed}}
         style={styles.detailed}
       />
     </View>
   );
}

module.exports = {
  Movie,
  Loaded,
  Loading,
  DetailedMovie
}
