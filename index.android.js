/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  ListView,
  TouchableHighlight
} from 'react-native';

const { setDispatch, dispatch } = require('./app/dispatch.js');
const { Movie, Loaded, Loading, DetailedMovie }  = require('./app/components.js');
const styles = require('./app/styles.js');

const MovieHighlight = (movie) => {
  return (
    <TouchableHighlight onPress={() => { dispatch(movie) }}>
      <View>
        <Movie movie={movie} />
      </View>
    </TouchableHighlight>

  )
}

const Movies = ({dataSource}) => {
  return (<ListView
        dataSource={dataSource}
        renderRow={MovieHighlight}
        style={styles.listView}
      />);
}

const REQUEST_URL = 'https://raw.githubusercontent.com/facebook/react-native/master/docs/MoviesExample.json';
const States = {
  loading: () => {
    return (<Loading />);
  },
  loaded: () => {
    return (<Loaded />);
  },
  all_movies: (state) => {
    return (<Movies dataSource={state.dataSource}/>);
  },
  detailed_movie: (state) => {
    return (<DetailedMovie movie={state.movie}/>);
  }
};
class AwesomeProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      state: States['loading']
    };
  }

  moviePressed(movie) {
    this.setState({ movie, state: States['detailed_movie'] });
  }

  componentDidMount() {
    setDispatch(this.moviePressed.bind(this));

    fetch(REQUEST_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData.movies),
          state: States['loaded'],
        });
        setTimeout(
          () => { this.setState({ state: States['all_movies'] })},
          1000);
      })
      .done();
  }

  render() {
    return this.state.state(this.state);
  }
}

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
